# README
This is a very basic Android app (and apologies, I'm no Android dev), that will respond to FCM notifications to the "wife-alert" topic and act suitably alarmed.

I would strongly advise not using anything about this app as reference material, there is way better stuff out there.
